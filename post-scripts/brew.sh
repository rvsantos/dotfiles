# Show Library folder
chflags nohidden ~/Library

# Show hidden files
defaults write com.apple.finder AppleShowAllFiles YES

# Show path bar
defaults write com.apple.finder ShowPathbar -bool true

# Show status bar
defaults write com.apple.finder ShowStatusBar -bool true

# Command line tools
xcode-select --install

# Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew tap homebrew/cask
brew tap homebrew/cask-fonts


# Brew pkg
brew install zsh
brew install curl
brew install git
brew install wget
brew install gpg2
brew install vivaldi
brew install kitty
brew install visual-studio-code-insiders
brew install exa
brew install fontconfig
brew install balenaetcher
brew install ripgrep fd
brew install tree pstree

# Brew fonts

# Oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# ASDF
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.1
