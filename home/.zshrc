# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

## Interactive shell ##

skip_global_compinit=1
zmodload zsh/mathfunc
autoload -Uz compaudit compinit run-help
if [[ -n ${ZDOTDIR}/.zcompdump(#qN.mh+24) ]]; then
  compinit;
else
  compinit -C;
fi

autoload -Uz run-help-git
export HELPDIR=/usr/local/share/zsh/helpfiles

# search on zshall man page
zman() { MANPAGER="less -g -s '+/^       "$1"'" man zshall }

autoload -Uz zmv
alias mmv='noglob zmv -W'

# Nicer history
HISTSIZE=100000
SAVEHIST=$HISTSIZE
REPORTTIME=10
#WORDCHARS=${WORDCHARS//[&=\/;\!#%\{]}
WORDCHARS='*?_[]~=&;!#$%^(){}'
HISTFILE=~/.zsh_history

# Uncomment following line if you want to disable autosetting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/Users/rvsantos/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# Caution: this setting can cause issues with multiline prompts (zsh 5.7.1 and newer seem to work)
# See https://github.com/ohmyzsh/ohmyzsh/issues/5765
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

#----------------------------------------------------------------- plugins ---
plugins=(
	git asdf yarn command-not-found z
	cp colored-man-pages common-aliases
	history sudo zsh-autosuggestions
	history-substring-search
)

# plugins for binaries
for p in fzf docker docker-compose gem gpg-agent pyenv redis-cli ssh-agent tmux; do
	(( $+commands[$p] )) && plugins+=($p)
done

# conditional plugins
(( $+commands[brew] ))      && plugins+=(brew osx gnu-utils)
(( $+commands[bundle] ))    && plugins+=(bundler)
(( $+commands[dpkg] ))      && plugins+=(ubuntu)
(( $+commands[git] ))       && plugins+=(git git-escape-magic github gitignore)
(( $+commands[go] ))        && plugins+=(golang)
(( $+commands[pacman] ))    && plugins+=(archlinux)
(( $+commands[rake] ))      && plugins+=(rake-fast)
(( $+commands[rvm] ))       && plugins+=(rvm)
(( $+commands[rg] ))        && plugins+=(ripgrep)
(( $+commands[systemctl] )) && plugins+=(systemd)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi
export EDITOR='vim'
 
# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#----------------------------------------------------------------- aliases ---
# Exa
if [[ -x /usr/local/bin/exa || -x /usr/bin/exa ]]; then
	alias ls="exa --long --header"
	alias la="exa --long --header --all"
	alias lt="exa --long --header --tree"
	alias lg="exa --long --header --all --git"
fi

# TMUX
unalias t
alias t=tmux
compdef _tmux t

alias vim='nvim'
alias rm='rm -i'
alias tree='tree -SA'
alias where='command -v'
alias mv='nocorrect mv -v'
alias cp='nocorrect cp -v'
alias mkdir='nocorrect mkdir'

# Brew update/upgrade
#alias brewup=‘brew update; brew upgrade; brew cleanup; brew doctor’

[[ -x /usr/bin/fdfind ]] && alias fd='fdfind'

#---------------------------------------------------------- suffix aliases ---
alias -s zip=zipinfo
alias -s tgz=gzcat
alias -s gz=gzcat
alias -s tbz=bzcat
alias -s bz2=bzcat

#---------------------------------------------------------- global aliases ---
alias -g A="| awk"
alias -g G="| grep"
alias -g GV="| grep -v"
alias -g H="| head"
alias -g L="| $PAGER"
alias -g P=' --help | less'
alias -g R="| ruby -e"
alias -g S="| sed"
alias -g T="| tail"
alias -g V="| vim -R -"
alias -g U=' --help | head'
alias -g W="| wc"
alias -g Z='| fzf'

#------------------------------------------------------ History completion ---
autoload -Uz history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey '\eOA' up-line-or-beginning-search
bindkey '\eOB' down-line-or-beginning-search
bindkey '^P' up-line-or-beginning-search
bindkey '^N' down-line-or-beginning-search

# edit command line in $EDITOR
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey "^X^E" edit-command-line

#---------------------------------------------------------------- ZSH opts ---
setopt auto_cd
setopt auto_list
setopt auto_param_keys
setopt auto_param_slash
setopt auto_pushd
setopt auto_resume
setopt brace_ccl
#setopt complete_aliases
setopt extended_glob
setopt extended_history
setopt hash_cmds
setopt hist_expand
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_no_store
setopt hist_reduce_blanks
setopt inc_append_history
setopt list_packed
setopt list_rows_first
setopt list_types
setopt long_list_jobs
setopt magic_equal_subst
setopt mark_dirs
setopt multios
setopt no_beep
setopt no_hup
#setopt no_clobber
setopt no_menu_complete
setopt numeric_glob_sort
setopt path_dirs
setopt print_eight_bit
setopt pushd_ignore_dups
setopt pushd_minus
setopt pushd_silent
setopt rm_star_wait
setopt share_history
setopt transient_rprompt

unsetopt promptcr
unsetopt correctall
unsetopt hist_verify
unsetopt print_exit_value

#--------------------------------------------------------------------- FZF ---
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
# Setting fd as the default source for fzf
# export FZF_DEFAULT_COMMAND='fd --type f'
export FZF_DEFAULT_COMMAND="fdfind --hidden --follow --exclude '.git' --exclude 'node_modules'"
export FZF_DEFAULT_OPTS="-m --cycle --preview '([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200'"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="fdfind --hidden --follow --exclude '.git' --exclude 'node_modules' --type d ."

# for more info see fzf/shell/completion.zsh
_fzf_compgen_path() {
    fd . "$1"
}
_fzf_compgen_dir() {
    fd --type d . "$1"
}

# like normal z when used with arguments but displays an fzf prompt when used without.
unalias z 2> /dev/null
z() {
    [ $# -gt 0 ] && _z "$*" && return
    cd "$(_z -l 2>&1 | fzf --height 40% --nth 2.. --reverse --inline-info +s --tac --query "${*##-* }" | sed 's/^[0-9,.]* *//')"
}

fzfrmimage() {
  docker image rm $(docker image ls | tail -n +2 | fzf | while read l; do echo $l | awk '{printf "%s:%s ", $1,$2}'; done)
}

#-----------------------------------------------------------------------------
listen() {
  lsof -nP -i":$1" | grep LISTEN
}

#-----------------------------------------------------------------------------
# YARN
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

#------------------------------------------------------------- p10k prompt ---
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
